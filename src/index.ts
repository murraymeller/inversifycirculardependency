import 'reflect-metadata';
import { ClubService } from './ClubService';
import { TYPES } from './types';
import { myContainer } from './inversify.config';

const clubService = myContainer.get<ClubService>(TYPES.ClubService);
