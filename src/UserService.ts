import {
  inject,
  injectable,
  LazyServiceIdentifer,
} from 'inversify';
import { TYPES } from './types';
import { IClubService } from './ClubService';

export interface IUserService {
}

@injectable()
export class UserService implements IUserService {
  private clubService;

  // From what I understand from the wiki page, LazyServiceIdentifier should allow us to use this circular dependency
  // https://github.com/inversify/InversifyJS/blob/master/wiki/circular_dependencies.md
  public constructor(
    @inject(new LazyServiceIdentifer(() => TYPES.ClubService)) clubService: IClubService,
  ) {
    this.clubService = clubService;
  }
}
