import {
  inject,
  injectable,
  LazyServiceIdentifer,
} from 'inversify';
import { TYPES } from './types';
import { IUserService } from './UserService';

export interface IClubService {
}

@injectable()
export class ClubService implements IClubService {
  private userService;

  // From what I understand from the wiki page, LazyServiceIdentifier should allow us to use this circular dependency
  // https://github.com/inversify/InversifyJS/blob/master/wiki/circular_dependencies.md
  public constructor(
    @inject(new LazyServiceIdentifer(() => TYPES.UserService)) userService: IUserService,
  ) {
    this.userService = userService;
  }
}
