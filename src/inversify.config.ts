import { Container } from 'inversify';
import { TYPES } from './types';
import {
  ClubService,
  IClubService,
} from './ClubService';
import {
  IUserService,
  UserService,
} from './UserService';

const myContainer = new Container();

myContainer.bind<IClubService>(TYPES.ClubService).to(ClubService);
myContainer.bind<IUserService>(TYPES.UserService).to(UserService);

export { myContainer };
