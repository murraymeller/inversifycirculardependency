# Inversify Circular Dependency

https://github.com/inversify/InversifyJS/blob/master/wiki/circular_dependencies.md

What I understand from the linked article is that, Inversify can handle circular dependencies. That is, it should be able to have a case where:

* ClubService depends on UserService
* UserService depends on ClubService

The article writes:

> If you have a circular dependency between two modules and you use the `@inject(SomeClass)` annotation. At runtime, one module will be parsed before the other and the decorator could be invoked with `@inject(SomeClass /* SomeClass = undefined*/)`. InversifyJS will throw an exception.

It then says there are two ways to overcome this limitation:

1. Use a `LazyServiceIdentifier`
1. Use a `@lazyInject` decorator

I have attempted a minimal code example of the `LazyServiceIdentifier` in this repository and have details below on how to produce the unexpected behaviour.    

## Steps to produce unexpected behaviour

* run `yarn install`
* run `yarn buildstart`

It will successfully compile but will crash when attempting to run. It gives the following error:    

```
/<PROJECT ROOT>/node_modules/inversify/lib/utils/serialization.js:65
            throw new Error(ERROR_MSGS.CIRCULAR_DEPENDENCY + " " + services);
            ^

Error: Circular dependency found: Symbol(ClubService) --> Symbol(UserService) --> Symbol(ClubService)
    at /<PROJECT ROOT>/node_modules/inversify/lib/utils/serialization.js:65:19
    at Array.forEach (<anonymous>)
    at circularDependencyToException (/<PROJECT ROOT>/node_modules/inversify/lib/utils/serialization.js:62:27)
    at /<PROJECT ROOT>/node_modules/inversify/lib/utils/serialization.js:68:13
    at Array.forEach (<anonymous>)
    at Object.circularDependencyToException (/<PROJECT ROOT>/node_modules/inversify/lib/utils/serialization.js:62:27)
    at Object.plan (/<PROJECT ROOT>/node_modules/inversify/lib/planning/planner.js:142:33)
    at /<PROJECT ROOT>/node_modules/inversify/lib/container/container.js:317:37
    at Container._get (/<PROJECT ROOT>/node_modules/inversify/lib/container/container.js:310:44)
    at Container.get (/<PROJECT ROOT>/node_modules/inversify/lib/container/container.js:230:21)
error Command failed with exit code 1.
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
error Command failed with exit code 1.
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
```

----

## Other

I have also considered using option two: Use a `@lazyInject` decorator. 

But my use case requires us to use request-scoped containers and it seems difficult to use decorators with a scoped container. 
